import React from "react";
import Header from '../Componenet/header';
import { Database } from "../Json/array";
import {Dropdown,InputGroup,FormControl,Button} from 'react-bootstrap'
export default function Remove(){
    const [value, setValue] = React.useState({count:'',name:''});
    const [num, setNum] = React.useState(''); // Setting default value

    let data = JSON.parse(localStorage.getItem('Detail'));
    if(!data){
        data= [ {
            code: '--',
            name:'--',
            count:'--',
        }]
    }
    const save=(code,count)=>{
        if(count<0){
            alert("Failed ");
            window.location.reload();
        }
        else{ for (var i = 0; i < data.length; i++) {
            if (data[i].code === code) {
                data[i].count = (data[i].count-count)<=0?0:data[i].count-count;
                
            }
          }
        localStorage.setItem('Detail', JSON.stringify(data));
        alert("Process Completed");
        window.location.reload();
    }
    }
    const handleChange  = (event) =>{
        setValue({ ...value, ['count']: event.target.value });
        
    }
    
    return(
            <>
               <Header path="remove_product" />
               <div style={{marginTop:'4%',marginLeft:'25%',marginRight:'25%'}}>
                   <Dropdown>
                       Select the Product Code <Dropdown.Toggle variant="dark" id="dropdown-basic">
                           {value.code}
                       </Dropdown.Toggle>
                       <hr />

                       <Dropdown.Menu>
                           {data.map(({code,name,count}) => (
                            
                           <Dropdown.Item key={code} onClick={()=>{setValue({count:count,name:name,code:code})
                           setNum(count)}}>{code}</Dropdown.Item>
                           ))}
                          

                       </Dropdown.Menu>
                   </Dropdown>
                   <InputGroup  className="mb-3">
                   <InputGroup.Text id="inputGroup-sizing-sm" style={{backgroundColor:'blue',color:'white'}}>{num+' - '} </InputGroup.Text>
    <FormControl aria-label="Default" type="number" aria-describedby="inputGroup-sizing-sm"  onChange={handleChange}/>
    <InputGroup.Text id="inputGroup-sizing-sm" style={{backgroundColor:'blue',color:'white'}}>=</InputGroup.Text>

  </InputGroup>
  <Button variant="primary" onClick={()=>save(value.code,value.count)} style={{width:'100%'}}>Save</Button>
                   </div>
           </>
    );
}