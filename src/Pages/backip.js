import React from "react";
import Form from 'react-bootstrap/Form';
import InputGroup from 'react-bootstrap/InputGroup';
import Button from 'react-bootstrap/Button';
import Header from '../Componenet/header';
import { Database } from "../Json/array";

export default function AddPdt(){
    
    let data = JSON.parse(localStorage.getItem('Detail'));
    const [db, setDb] = React.useState(data?data:Database);
    const [id, setInputDisable] = React.useState({status:false,name:''
    });
    const [num, setNum] = React.useState(0); // Setting default value
    const [formData, updateFormData] = React.useState({ });
    const [saveDatacode, submitDatacode] = React.useState();
    
    // React.useEffect(() => {
       
    // }, [Database]);

    const save=()=>{
        setNum(num>0?num-1:0)
        if(!data){
        
        db.push(formData)
        localStorage.setItem('Detail', JSON.stringify(db));
        updateFormData('')

    }else{
        for (var i = 0; i < db.length; i++) {
            if (db[i].code === formData.code) {
                
                db[i].count = parseInt(db[i].count)+parseInt(formData.count);
                
            }
        }
        
        localStorage.setItem('Detail', JSON.stringify(db));
        updateFormData('')
    }
    }

    const handleChange = (prop) => (event) =>{
   
        updateFormData({ ...formData, [prop]: event.target.value });
        for (var i = 0; i < db.length; i++) {
            if (db[i].code === formData.code) {
                setInputDisable({status:true,name:db[i].name})
               
                
            }
            else{
                setInputDisable({status:false,name:db[i].name})

            }
        }
    }
    const handleN = (b) => {
        localStorage.setItem('count', b)
        setNum(b)
    };
    
    return(
                <>
                <Header/>
                <InputGroup className="mb-3" style={{marginTop:40,width:'50%',marginLeft:'20%'}}>
                <Form.Control
                type="number"
                placeholder="enter number of product"
                aria-label="Recipient's username"
                onChange={(e)=>handleN(e.target.value)}
                aria-describedby="basic-addon2"
                />
                <InputGroup.Text id="basic-addon2" ><div >Submit </div></InputGroup.Text>

            </InputGroup>
   <div style={{marginBottom:'12%',width:'50%', marginLeft:'20%'
}}>
                            <h4><b>Add Product</b></h4><hr/>
                        <Form.Label htmlFor="inputPassword5"  >Product Code</Form.Label>
                        <Form.Control type="text" required value={formData?formData.code:''} onChange={handleChange('code')}  placeholder="Enter the Product Code "aria-describedby="passwordHelpBlock" />
                        <Form.Label htmlFor="inputPassword5" >Product Name</Form.Label>
                        {
                            id.status===true?
                                <div><b>{id.name}</b></div>:
                                <Form.Control type="text" required value={formData?formData.name:''} onChange={handleChange('name')} placeholder="Enter the name of Product "aria-describedby="passwordHelpBlock" />

                        }
                        <Form.Label htmlFor="inputPassword5"  >Quantity</Form.Label>
                        <Form.Control type="number" required value={formData?formData.count:''}  onChange={handleChange('count')}  placeholder=" Quantity "aria-describedby="passwordHelpBlock" />
                        <Button variant="primary" size="lg" disabled={num<=0?true:false} onClick={()=>save()}>
                          {` Submit (`+num+`)`}
                        </Button>
                        </div>  
              
        </>
    );
}