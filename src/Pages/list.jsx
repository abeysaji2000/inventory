import React from "react";
import Table from 'react-bootstrap/Table';
import Header from '../Componenet/header';

export default function List(){
    let data = JSON.parse(localStorage.getItem('Detail'));
    if(!data){
        data=[ {
            code: '--',
            name:'--',
            count:'--',
        }]
    }
    console.log(data)
    return(
            <><Header path="list"/>
                    <div style={{marginTop:'4%',marginLeft:'25%',marginRight:'25%'}}>
                <Table striped bordered hover >
            <thead>
                <tr>
            
                <th>Code</th>
                <th>Name</th>
                <th>Count</th>
                </tr>
            </thead>
            <tbody>
            
                {data.map(({code,name,count}) => (
                        <tr>
                        <td>{code}</td>
                        <td>{name}</td>
                        <td>{count}</td>
                    
                    </tr>
                    ))}
                
            </tbody>
            </Table>
        </div>
        </>
    );
}