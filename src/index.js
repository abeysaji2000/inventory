import React, { Suspense } from "react";
import ReactDOM from "react-dom";

import App from "./App";
import Mycomponent from './Spinner';
ReactDOM.render( 
   
    <Suspense fallback={<Mycomponent/>}>
    
            <App /> 
   
    </Suspense>,
    document.getElementById("root")
);

