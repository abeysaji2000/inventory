import React from "react";
import Nav from 'react-bootstrap/Nav'

export default function Home(props){
    // eslint-disable-next-line
    return(
        <><Nav fill 
        variant="tabs" 
        defaultActiveKey="/"
        activeKey={props.path}>
        <Nav.Item>
          <Nav.Link href="/"> Add Products</Nav.Link>
        </Nav.Item>
        <Nav.Item>
          <Nav.Link href={`/list`} eventKey="list">List Products</Nav.Link>
        </Nav.Item>
        <Nav.Item>
          <Nav.Link href={`/remove_product`} eventKey="remove_product">Remove Product</Nav.Link>
        </Nav.Item>
        
      </Nav>
        </>
    );

}