import "../App.css";
import { lazy } from "react";

const List = lazy(() => import("../Pages/list"));
const Add = lazy(() => import("../Pages/addproduct"));
const Remove = lazy(() => import("../Pages/delete"));

const RoutesArray = [
    {
        path: "/*",
        element:<Add />,
        ignore: true,
        routeP: "/*",
    },
    {
        path: "/list",
        element:<List />,
        ignore: true,
        routeP: "/list",
    },{
        path: "/remove_product",
        element:<Remove />,
        ignore: true,
        routeP: "/remove_product",
    },
];

export default RoutesArray;
