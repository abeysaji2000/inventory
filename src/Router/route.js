import { useRoutes } from "react-router-dom";
import RoutesArray from "./Routes";

const Router = () => {
  const routes = useRoutes(RoutesArray);
 
  return routes;
};

export default Router;
