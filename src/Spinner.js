import React from 'react';
import { useState, useEffect } from 'react';
import { Spinner  } from 'react-bootstrap';
export default function Spinners(prop) {
    const [windowDimensions, setWindowDimensions] = useState(getWindowDimensions());

    function getWindowDimensions() {
      const { innerWidth: width, innerHeight: height } = window;
      return {
        width,
        height
      };
    }
    
     function useWindowDimensions() {
    
      useEffect(() => {
        function handleResize() {
          setWindowDimensions(getWindowDimensions());
        }
    
        window.addEventListener('resize', handleResize);
        return () => window.removeEventListener('resize', handleResize);
      }, []);
    
      return windowDimensions;
    }
    const { height} = useWindowDimensions();

    return (
     <div style={{backgroundColor : 'white',
     display: "flex", 
     justifyContent: "center",
     alignItems: "center" ,
     height : prop.home?150:height
     }}>

<Spinner animation="grow" />
    </div>
    );

}

